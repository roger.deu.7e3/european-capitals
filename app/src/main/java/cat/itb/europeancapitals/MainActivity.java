package cat.itb.europeancapitals;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private TextView question, progressText, hintText, scoreText;
    private ProgressBar progressBar;
    private Button buttonFirst, buttonSecond, buttonThird, buttonFourth, hintButton;
    AlertDialog.Builder dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        question = findViewById(R.id.questionText);
        progressBar = findViewById(R.id.progressBar);
        buttonFirst = findViewById(R.id.buttonFirstChoice);
        buttonSecond = findViewById(R.id.buttonSecondChoice);
        buttonThird = findViewById(R.id.buttonThirdChoice);
        buttonFourth = findViewById(R.id.buttonFourthChoice);
        progressText = findViewById(R.id.progressText);
        hintText = findViewById(R.id.hintText);
        hintButton = findViewById(R.id.hintButton);
        scoreText = findViewById(R.id.scoreText);

        final QuestionViewModel qvm = new ViewModelProvider(this).get(QuestionViewModel.class);
        final Question[] questions = qvm.questionsModel;
        final int[] questionOrder = qvm.questionOrder;

        dialog = new AlertDialog.Builder(this);
        qvm.progressBar = progressBar;

        if (qvm.contador == 0) {
            qvm.iniciarProgress();
            qvm.questionNumber = questionOrder[qvm.j];
            qvm.contador++;
            initApp(qvm,questions,questionOrder);
            qvm.setQuestion(question,buttonFirst,buttonSecond,buttonThird,buttonFourth);
            scoreText.setText(String.valueOf("Tu puntuacion es de: "+qvm.score+" puntos."));
        } else {
            if (qvm.hintcounter >= 2 && qvm.hint == 1) {
                hintText.setText("La resposta correcta es: "+qvm.capitals[qvm.questionNumber]);
            }
            qvm.recoverQuestion(question,buttonFirst,buttonSecond,buttonThird,buttonFourth);
            progressText.setText("Pregunta: "+qvm.contador+" de: "+qvm.numberOfQuestions);
            scoreText.setText(String.valueOf("Tu puntuacion es de: "+qvm.score+" puntos."));
        }
        if (qvm.hint == 1) {
            qvm.hintSetText(hintText,hintButton);
        }

        if (progressBar.getProgress() == 10) {
            checkAnswer(qvm,questions,"no",questionOrder);
        }

        progressBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(qvm,questions,"no",questionOrder);
            }
        });

        buttonFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(qvm,questions,buttonFirst.getText().toString(),questionOrder);
            }
        });

        buttonSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(qvm,questions,buttonSecond.getText().toString(),questionOrder);
            }
        });

        buttonThird.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(qvm,questions,buttonThird.getText().toString(),questionOrder);
            }
        });

        buttonFourth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(qvm,questions,buttonFourth.getText().toString(),questionOrder);
            }
        });

        hintButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qvm.hintSetText(hintText,hintButton);
                qvm.hint = 1;
            }
        });


    }

    public void checkAnswer(QuestionViewModel qvm, Question[] questions, String option, int[] questionOrder) {
        if (questions[qvm.questionNumber].getCapital().equalsIgnoreCase(option)) {
            Toast.makeText(this, "Correcte!!!", Toast.LENGTH_SHORT).show();
            if (qvm.hint == 0) {
                qvm.score += 1;
            }
        } else if (progressBar.getProgress()==10){
            Toast.makeText(MainActivity.this, "S'ha acabat el temps'... la resposta correcta era: "+questions[qvm.questionNumber].getCapital(), Toast.LENGTH_SHORT).show();
            qvm.score -= 0.5;
        } else {
            Toast.makeText(this, "Incorrecte... la resposta correcte era: "+questions[qvm.questionNumber].getCapital(), Toast.LENGTH_SHORT).show();
            qvm.score -= 0.5;
        }
        qvm.questionNumber = questionOrder[qvm.j]-1;
        qvm.j++;
        //progressBar.setProgress(0);
        qvm.contadorPreguntes++;
        qvm.contador++;
        if (qvm.contadorPreguntes==49) {
            comprobarFin(qvm);
        }
        scoreText.setText(String.valueOf("Tu puntuacion es de: "+qvm.score+" puntos."));
        hintButton.setEnabled(true);
        qvm.setQuestion(question,buttonFirst,buttonSecond,buttonThird,buttonFourth);
        qvm.myCountDownTimer.cancel();
        qvm.iniciarProgress();
        hintText.setText("");

        qvm.hint = 0;
        progressText.setText("Pregunta: "+qvm.contador+" de: "+qvm.numberOfQuestions);
    }

    public void initApp(QuestionViewModel qvm, Question[] questions,int[] questionOrder) {
        qvm.capitals = getResources().getStringArray(R.array.capitalsArray);
        qvm.countries = getResources().getStringArray(R.array.countriesArray);
        qvm.fillQuestionOrder();
        qvm.fillQuestionModell(questions);
        progressText.setText("Pregunta: "+qvm.contador+" de: "+qvm.numberOfQuestions);
    }

    public void comprobarFin(QuestionViewModel qvm) {
        double puntuacio = (qvm.score * 100)/qvm.numberOfQuestions;
        String points = String.format("%.2f",puntuacio);
        dialog.setTitle("Aquesta es la teva puntuacio");
        dialog.setMessage("Has obtingut una puntuació de: "+points+" sobre 100");
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                finish();
            }
        });
        dialog.show();
    }
}
