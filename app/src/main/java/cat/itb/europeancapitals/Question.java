package cat.itb.europeancapitals;

public class Question {
    private String questionText;
    private String capital;

    public Question(String questionText, String capital) {
        this.questionText = questionText;
        this.capital = capital;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }
}
