package cat.itb.europeancapitals;

import android.app.ProgressDialog;
import android.content.res.Resources;
import android.os.CountDownTimer;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.lifecycle.ViewModel;

public class QuestionViewModel extends ViewModel {

    int numberOfQuestions = 49, contador = 0, hintcounter = 0, randomnumber, button1, button2, button3, hint = 0, contadorPreguntes = 0, j = 0, progress;
    String[] countries = new String[numberOfQuestions];
    String[] capitals = new String[numberOfQuestions];
    int[] questionOrder = new int[numberOfQuestions];
    Question[] questionsModel = new Question[numberOfQuestions];
    int questionNumber;
    double  score=0;
    boolean checker = false, checkAnswer = false;
    ProgressBar progressBar;
    MyCountDownTimer myCountDownTimer;


    public void fillQuestionOrder() {
        boolean marca = false;
        for (int i = 0; i < questionOrder.length; i++) {
            int numero;
            numero = (int) (Math.random() * 50);
            for (int j = 0; j < questionOrder.length; j++) {
                if (numero==questionOrder[j]){
                    marca=true;
                }
            }
            if (!marca) {
                questionOrder[i] = numero;
            } else {
                i--;
                marca=false;
            }
        }
    }

    public void fillQuestionModell(Question[] questionsModel) {
        int contador = 0;
        while (contador < questionsModel.length) {
            questionsModel[contador] = new Question(countries[questionOrder[contador]],capitals[questionOrder[contador]]);
            contador++;
        }
    }

    public void hintSetText(TextView hintText, Button hintButton) {
        if (hintcounter <= 2) {
            hintText.setText("La resposta correcta es: "+questionsModel[questionNumber].getCapital());
            if (hint == 0) {
                hintcounter++;
            }
            if (hintcounter == 3) {
                hintButton.setEnabled(false);
            }
        }
    }

    public void setQuestionText(TextView question) {
        question.setText(questionsModel[questionNumber].getQuestionText());
        checkAnswer = false;
    }

    public void recoverQuestion(TextView question, Button buttonFirst,Button buttonSecond,Button buttonThird,Button buttonFourth) {
        question.setText(questionsModel[questionNumber].getQuestionText());
        checkAnswer = true;
        fillTextButtons(buttonFirst,buttonSecond,buttonThird,buttonFourth);
    }

    public void fillTextButtons(Button buttonFirst,Button buttonSecond,Button buttonThird,Button buttonFourth) {
        if (!checkAnswer || contador == 0) {
            randomnumber = randomNumber();
            button1 = randomNumber();
            button2 = randomNumber();
            button3 = randomNumber();
        }
        if (randomnumber <= 10) {
            buttonFirst.setText(capitals[button1]);
            buttonSecond.setText(capitals[button2]);
            buttonThird.setText(capitals[button3]);
            buttonFourth.setText(questionsModel[questionNumber].getCapital());
        } else if (randomnumber <= 20) {
            buttonFirst.setText(questionsModel[questionNumber].getCapital());
            buttonSecond.setText(capitals[button2]);
            buttonThird.setText(capitals[button3]);
            buttonFourth.setText(capitals[button1]);
        } else if (randomnumber <= 30) {
            buttonFirst.setText(capitals[button1]);
            buttonSecond.setText(questionsModel[questionNumber].getCapital());
            buttonThird.setText(capitals[button3]);
            buttonFourth.setText(capitals[button2]);
        } else {
            buttonFirst.setText(capitals[button1]);
            buttonSecond.setText(capitals[button2]);
            buttonThird.setText(questionsModel[questionNumber].getCapital());
            buttonFourth.setText(capitals[button3]);
        }

    }

    public void setQuestion(TextView question, Button buttonFirst,Button buttonSecond,Button buttonThird,Button buttonFourth) {
        setQuestionText(question);
        fillTextButtons(buttonFirst,buttonSecond,buttonThird,buttonFourth);
    }

    public int randomNumber() {
        int numero = (int) (Math.random()*numberOfQuestions);
        while (!checker) {
            if (numero == questionNumber || numero == button1 || numero == button2 || numero == button3) {
                numero = (int) (Math.random()*numberOfQuestions);
            } else {
                checker = true;
            }
        }
        checker = false;
        return numero;
    }

    public class MyCountDownTimer extends CountDownTimer {

        public MyCountDownTimer(long millisUntilFinished, long countDownInterval) {
            super(millisUntilFinished,countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            int progress = (int) (millisUntilFinished/1000);
            progressBar.setProgress(progressBar.getMax()-progress);
        }

        @Override
        public void onFinish() {
            progressBar.setProgress(progressBar.getMax());
            progressBar.performClick();
        }
    }

    public void iniciarProgress() {
        myCountDownTimer = new MyCountDownTimer(10000,1000);
        myCountDownTimer.start();
    }

}
